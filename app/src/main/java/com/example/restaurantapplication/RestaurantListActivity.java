package com.example.restaurantapplication;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.restaurantapplication.ModelResponse.BookingResponse;
import com.example.restaurantapplication.ModelResponse.RestaurantResponse;
import com.example.restaurantapplication.adapter.RestaurantListAdapter;
import com.example.restaurantapplication.databinding.ActivityRestaurantListBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantListActivity extends AppCompatActivity {
    private Dialog loadingDialog;
    ActivityRestaurantListBinding binding;
    RestaurantListAdapter restaurantListAdapter;
    List<RestaurantResponse> restaurantList;
    SharedPrefManager sharedPrefManager;
    private static final int REQ_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRestaurantListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.myToolbar);

        sharedPrefManager = new SharedPrefManager(getApplicationContext());

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);

        // Pending intent Alarm Manager
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent1 = new Intent(this,AlarmReceiver.class);
        PendingIntent pendingIntent1 = PendingIntent.getBroadcast(this,REQ_CODE,intent1,PendingIntent.FLAG_UPDATE_CURRENT);

        // Setting Loading layout
        loadingDialog = new Dialog(this);
        loadingDialog.setContentView(R.layout.loading);
        loadingDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        loadingDialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.rounded_corner));
        loadingDialog.setCancelable(false);

        // Getting authentication Token.
        SharedPreferences pref = getSharedPreferences("AuthToken",MODE_PRIVATE);
        String authToken = pref.getString("token",null);
        String location = pref.getString("selected_location",null);

        loadingDialog.show(); // showing loading screen
        Call<List<RestaurantResponse>> call = RetrofitClient.getInstance().getRetrofitInterface().fetchRestaurants(authToken, location);
        call.enqueue(new Callback<List<RestaurantResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<RestaurantResponse>> call, @NonNull Response<List<RestaurantResponse>> response) {
                if (response.isSuccessful()) {
                    restaurantList = new ArrayList<>();

                    List<RestaurantResponse> restaurants = response.body();
                    restaurantList.addAll(restaurants);

                    restaurantListAdapter = new RestaurantListAdapter(getApplicationContext(),restaurantList);
                    binding.recyclerView.setAdapter(restaurantListAdapter);

                    new Handler().postDelayed(() -> {
                        // Dismiss the loading dialog after 0.5 second
                        loadingDialog.dismiss();
                    }, 500);

                    restaurantListAdapter.setOnItemClickListener(position -> {
                        Intent intent = new Intent(RestaurantListActivity.this, RestaurantDetailActivity.class);
                        intent.putExtra("id",restaurantList.get(position).getId());
                        intent.putExtra("name", restaurantList.get(position).getName());
                        intent.putExtra("location", restaurantList.get(position).getLocation());
                        intent.putExtra("description", restaurantList.get(position).getCuisineType());
                        intent.putExtra("image", restaurantList.get(position).getImage());
                        startActivity(intent);
                    });
                    // Set Notification after a particular time period.
                    // long triggerTime1 = SystemClock.elapsedRealtime() + 10000; // 10 seconds
                    // alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,triggerTime1, pendingIntent1);

                    // setRepeating for repeating the notification at particular interval
                    long intervalMillis = 60 * 60 * 1000; // 10 seconds
                    long triggerTime = SystemClock.elapsedRealtime() + intervalMillis;
                    alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,triggerTime,intervalMillis, pendingIntent1);

                } else {
                    loadingDialog.dismiss();
                    Toast.makeText(RestaurantListActivity.this, "Error occurred!: " + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<RestaurantResponse>> call, Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(RestaurantListActivity.this, "Cannot connect to server", Toast.LENGTH_SHORT).show();
            }
        });

        SharedPreferences isBooked = getSharedPreferences("confirmationDetails",MODE_PRIVATE);
        SharedPreferences getUserId = getSharedPreferences("userDetails",MODE_PRIVATE);
        int userID = getUserId.getInt("id",0);

        binding.fabConfirmBooking.setOnClickListener(view -> {
            Call<BookingResponse> call1 = RetrofitClient
                    .getInstance()
                    .getRetrofitInterface()
                    .getBookingDetails(userID);
            call1.enqueue(new Callback<BookingResponse>() {
                @Override
                public void onResponse(Call<BookingResponse> call, Response<BookingResponse> response) {
                    if(response.isSuccessful()){
                        BookingResponse bookingResponse = response.body();
                        Intent confirmationIntent = new Intent(RestaurantListActivity.this, BookingSummaryActivity.class);
                        confirmationIntent.putExtra("id",bookingResponse.getId());
                        confirmationIntent.putExtra("restaurantName",bookingResponse.getRestaurantName());
                        confirmationIntent.putExtra("customerName",bookingResponse.getCustomerName());
                        confirmationIntent.putExtra("date",bookingResponse.getBookingDate());
                        confirmationIntent.putExtra("start_time",bookingResponse.getStartTime());
                        confirmationIntent.putExtra("end_time",bookingResponse.getEndTime());
                        confirmationIntent.putExtra("no_of_person",bookingResponse.getNumGuests());
                        confirmationIntent.putExtra("contactNumber",bookingResponse.getContactNumber());
                        startActivity(confirmationIntent);
                        Toast.makeText(RestaurantListActivity.this, "Your last Booking.", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        try {
                            assert response.errorBody() != null;
                            JSONObject errorObject = new JSONObject(response.errorBody().string());
                            if (errorObject.has("error")) {
                                String errorMessage = errorObject.getString("error");
                                Toast.makeText(RestaurantListActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RestaurantListActivity.this, "Error Code: " + response.code(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<BookingResponse> call, Throwable t) {
                    Toast.makeText(RestaurantListActivity.this, "Network error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        });

        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                showExitDialog();
            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setBackgroundResource(R.drawable.edit_text_rounded_corner);
        searchView.setQueryHint("Cuisine or Restaurant");

        // Implement SearchView listener if needed
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                restaurantListAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                restaurantListAdapter.filter(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.action_logout){
            showLogoutDialog();
        } else if(item.getItemId() == R.id.action_location){
            Intent locationIntent = new Intent(RestaurantListActivity.this, LocationListActivity.class);
            startActivity(locationIntent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit");
        builder.setMessage("Are you sure you want to exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Handle the exit action
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Dismiss the dialog
                dialog.dismiss();
            }
        });

        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Handle the exit action
                sharedPrefManager.logout();

                // Clear previous Booking Details.
                SharedPreferences pref1 = getSharedPreferences("confirmationDetails",MODE_PRIVATE);
                SharedPreferences.Editor editor = pref1.edit();
                editor.clear();
                editor.apply();

                Intent intent = new Intent(RestaurantListActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                Toast.makeText(RestaurantListActivity.this, "You have been logged out", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Dismiss the dialog
                dialog.dismiss();
            }
        });

        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
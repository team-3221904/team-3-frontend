package com.example.restaurantapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.restaurantapplication.databinding.ActivityLocationListBinding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationListActivity extends AppCompatActivity {
    private ActivityLocationListBinding binding;
    private List<String> locations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLocationListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        locations = new ArrayList<>();

        binding.searchLocationInput.requestFocus(); // To display keyboard when user comes to this activity.

        SharedPreferences pref = getSharedPreferences("AuthToken",MODE_PRIVATE);
        String authToken = pref.getString("token",null);

        Call<List<String>> call = RetrofitClient.getInstance().getRetrofitInterface().fetchLocations(authToken);
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (response.isSuccessful()) {
                    locations = response.body();
                    updateAutoCompleteAdapter(locations);
                } else {
                    Toast.makeText(LocationListActivity.this, "Error Code: " + response.code(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                Toast.makeText(LocationListActivity.this, "Network error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        binding.searchLocationInput.setOnItemClickListener((parent, view, position, id) -> {
//            String selectedLocation = (String) parent.getItemAtPosition(position);
//            binding.searchLocationInput.setText(selectedLocation);
            String searchTerm = binding.searchLocationInput.getText().toString().trim();
            if (searchTerm.isEmpty()) {
                binding.searchLocationInput.setError("Please enter location");
                return;
            }
            setUpRecyclerView(searchTerm);
        });

        binding.btnSearchLocation.setOnClickListener(v -> {
            String searchTerm = binding.searchLocationInput.getText().toString().trim();
            if (searchTerm.isEmpty()) {
                binding.searchLocationInput.setError("Please enter location");
                return;
            }
//            setUpRecyclerView(searchTerm);
        });
    }

    private void updateAutoCompleteAdapter(List<String> locations) {
        ArrayAdapter<String> autoCompleteAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, locations);
        binding.searchLocationInput.setAdapter(autoCompleteAdapter);
        binding.searchLocationInput.setThreshold(3); // Show suggestions after the first character
    }

    private void setUpRecyclerView(String searchTerm) {
        // Perform location search and get filtered locations
        List<String> filteredLocations = performLocationSearch(searchTerm);

        // Check if the filteredLocations list is not empty
        if (!filteredLocations.isEmpty()) {

            SharedPreferences pref = getSharedPreferences("AuthToken",MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("selected_location",searchTerm);
            editor.apply();

            // If the list is not empty, create an intent to start the RestaurantListActivity
            Intent intent = new Intent(LocationListActivity.this, RestaurantListActivity.class);
            intent.putExtra("selected_location", searchTerm);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "No restaurants found in " + searchTerm, Toast.LENGTH_SHORT).show();
        }
    }
    private List<String> performLocationSearch(String searchTerm) {
        List<String> filteredLocations = new java.util.ArrayList<>();
        for (String location : locations) {
            if (location.equalsIgnoreCase(searchTerm)) {
                filteredLocations.add(location);
                break;
            }
        }
        return filteredLocations;
    }
}
package com.example.restaurantapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.example.restaurantapplication.ModelResponse.ReservationResponse;
import com.example.restaurantapplication.databinding.ActivityBookingReviewBinding;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingReviewActivity extends AppCompatActivity {
    private ActivityBookingReviewBinding binding;
    private Intent confirmationIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBookingReviewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // For notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "channel_id",
                    "Your Channel Name",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        // HashMap to represent booking details
        HashMap<String, Object> map = new HashMap<>();

        String userName, contactNumber, restaurantName;
        // Get data from the intent
        Intent intent = getIntent();
        if (intent != null) {
            SharedPreferences userDetails = getSharedPreferences("userDetails", MODE_PRIVATE);
            userName = userDetails.getString("name", null);
            contactNumber = userDetails.getString("contactNumber", null);

            int id = intent.getIntExtra("id", 0);
            restaurantName = intent.getStringExtra("restaurantName");
            String date = intent.getStringExtra("date");
            String timeSlot = intent.getStringExtra("timeSlot");
            String image = intent.getStringExtra("image");
            String noOfPerson = intent.getStringExtra("numberOfPerson");

            // Display data in TextViews
            Picasso.get().load(image).into(binding.restaurantImage);
            binding.restaurantName.setText(restaurantName);
            binding.customerName.setText(userName);
            binding.date.setText(date);
            binding.timeSlot.setText(timeSlot);
            binding.numberOfPerson.setText(noOfPerson);
            binding.contactNumber.setText(contactNumber);

            int dashIndex = timeSlot.indexOf("-");
            String startTime = timeSlot.substring(0, dashIndex).trim().replaceAll("[^\\d:]", "");

            // Extract end time
            String endTime = timeSlot.substring(dashIndex + 1).trim().replaceAll("[^\\d:]", "");

            map.put("restaurantId", id);
            map.put("startTime", startTime);
            map.put("endTime", endTime);
            map.put("numberOfPeople", Integer.parseInt(noOfPerson));
            map.put("reservationDate", date);
            map.put("customerName", userName);
            map.put("contactNumber", contactNumber);

            SharedPreferences pref = getSharedPreferences("AuthToken", MODE_PRIVATE);
            String authToken = pref.getString("token", null);

            binding.btnConfirmBooking.setOnClickListener(v -> {

                Call<ReservationResponse> call = RetrofitClient
                        .getInstance()
                        .getRetrofitInterface()
                        .confirmBooking(authToken, map);
                call.enqueue(new Callback<ReservationResponse>() {
                    @Override
                    public void onResponse(Call<ReservationResponse> call, Response<ReservationResponse> response) {
                        ReservationResponse reservationResponse = response.body();
                        if (response.isSuccessful()) {
                            confirmationIntent = new Intent(BookingReviewActivity.this, BookingSummaryActivity.class);
                            confirmationIntent.putExtra("id", reservationResponse.getId());
                            confirmationIntent.putExtra("restaurantName", restaurantName);
                            confirmationIntent.putExtra("customerName", reservationResponse.getCustomerName());
                            confirmationIntent.putExtra("date", reservationResponse.getBookingDate());
                            confirmationIntent.putExtra("start_time", startTime);
                            confirmationIntent.putExtra("end_time", endTime);
                            confirmationIntent.putExtra("no_of_person", reservationResponse.getNumGuests());
                            confirmationIntent.putExtra("contactNumber", reservationResponse.getContactNumber());
                            confirmationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);  // To remove review and detailed activity from stack
                            startActivity(confirmationIntent);

                            SharedPreferences pref1 = getSharedPreferences("confirmationDetails",MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref1.edit();
                            editor.putInt("id",reservationResponse.getId());
                            editor.putBoolean("isBooking",true);
                            editor.apply();
                            Toast.makeText(BookingReviewActivity.this, "Booking confirmed!", Toast.LENGTH_SHORT).show();
                            showNotification("Booking Confirmed", "Your booking has been confirmed. Click to view details.");
                            finish();
                        } else {
                            try {
                                JSONObject errorObject = new JSONObject(response.errorBody().string());
                                if (errorObject.has("error")) {
                                    String errorMessage = errorObject.getString("error");
                                    Toast.makeText(BookingReviewActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(BookingReviewActivity.this, "Error Code: " + response.code(), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ReservationResponse> call, Throwable t) {
                        Toast.makeText(BookingReviewActivity.this, "Network error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            });
        }
        binding.btnEdit.setOnClickListener(view -> finish());
    }

    // Method to show a notification
    private void showNotification(String title, String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "channel_id")
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(getPendingIntent())
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        notificationManager.notify(1, builder.build());
    }

    // Method to create a pending intent for the notification
    private PendingIntent getPendingIntent() {
//        Intent notificationIntent = new Intent(this, BookingSummaryActivity.class);
        confirmationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return PendingIntent.getActivity(this, 0, confirmationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
package com.example.restaurantapplication;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.restaurantapplication.ModelResponse.LoginResponse;
import com.example.restaurantapplication.databinding.ActivityMainBinding;

import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    SharedPrefManager sharedPrefManager;
    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // To Save User Data
        sharedPrefManager = new SharedPrefManager(getApplicationContext());

        binding.btnSignIn.setOnClickListener(v -> {
            String email = binding.email.getText().toString();
            String password = binding.password.getText().toString();

            if (email.isEmpty()){
                binding.email.requestFocus();
                binding.email.setError("Please enter your email");
                return;
            }
            // Check email format.
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                binding.email.requestFocus();
                binding.email.setError("Please Enter Correct Email");
                return;
            }

            if (password.isEmpty()) {
                binding.password.requestFocus();
                Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
                return;
            }

            HashMap<String, String> map = new HashMap<>();
            map.put("email", email);
            map.put("password", password);

            Call<LoginResponse> call = RetrofitClient
                    .getInstance()
                    .getRetrofitInterface()
                    .executeLogin(map);

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                    LoginResponse loginResponse = response.body();
                    if (response.isSuccessful()) {
                        assert loginResponse != null;
                        sharedPrefManager.saveUser(loginResponse.getUser());

                        // Saving Authentication Token
                        SharedPreferences pref = getSharedPreferences("AuthToken", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("token", loginResponse.getToken());
                        editor.apply();

                        Intent intent = new Intent(MainActivity.this, LocationListActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        Toast.makeText(MainActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            assert response.errorBody() != null;
                            JSONObject errorObject = new JSONObject(response.errorBody().string());
                            if (errorObject.has("error")) {
                                String errorMessage = errorObject.getString("error");
                                Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MainActivity.this, "Error Code: " + response.code(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                    Toast.makeText(MainActivity.this, "Network error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        });

        binding.btnSignUp.setOnClickListener(v -> {
            Intent signUpIntent = new Intent(MainActivity.this, SignUpActivity.class);
            startActivity(signUpIntent);
            Toast.makeText(MainActivity.this, "Redirecting to Sign Up", Toast.LENGTH_SHORT).show();
        });

        binding.closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExitDialog();
            }
        });

        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                showExitDialog();
            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);
    }

    // Exit Dialog
    private void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit");
        builder.setMessage("Are you sure you want to exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Handle the exit action
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Dismiss the dialog
                dialog.dismiss();
            }
        });

        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // After user login.
    protected void onStart() {
        super.onStart();

        SharedPreferences pref = getSharedPreferences("AuthToken",MODE_PRIVATE);
        String cityName = pref.getString("selected_location",null);

        if (sharedPrefManager.isLoggedIn() && cityName!=null) {
            Intent intent = new Intent(MainActivity.this, RestaurantListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        else if(sharedPrefManager.isLoggedIn()){
            Intent intent = new Intent(MainActivity.this,LocationListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

    }
}
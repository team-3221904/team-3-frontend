package com.example.restaurantapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.restaurantapplication.ModelResponse.SlotResponse;
import com.example.restaurantapplication.adapter.SlotAdapter;
import com.example.restaurantapplication.databinding.ActivityRestaurantDetailBinding;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantDetailActivity extends AppCompatActivity implements SlotAdapter.OnSlotClickListener {
    ActivityRestaurantDetailBinding binding;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    List<SlotResponse.Slot> slots;
    String selectedDate;
    SlotAdapter slotAdapter;
    int selectedSlotPosition = RecyclerView.NO_POSITION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRestaurantDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.myToolbar);

        Intent intent = getIntent();
        if (intent != null) {
            String name = intent.getStringExtra("name");
            String location = intent.getStringExtra("location");
            String description = intent.getStringExtra("description");
            String image = intent.getStringExtra("image");
            binding.restaurantName.setText(name);
            binding.cityName.setText(location);
            binding.description.setText(description);
            Picasso.get().load(image).into(binding.detailImage);
        }

        binding.datePick.setOnClickListener(v -> {
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(
                    RestaurantDetailActivity.this,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    dateSetListener,
                    year, month, day
            );

            // Set minimum date (current date - 1 day)
            Calendar minDate = Calendar.getInstance();
            minDate.add(Calendar.DAY_OF_MONTH, -1); // Subtract one day
            dialog.getDatePicker().setMinDate(minDate.getTimeInMillis());

            // Set maximum date (e.g., one week from current date)
            Calendar maxDate = Calendar.getInstance();
            maxDate.add(Calendar.DAY_OF_MONTH, 7); // Add one week
            dialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        });

        dateSetListener = (view, year, month, dayOfMonth) -> {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth);
            selectedDate = dateFormat.format(calendar.getTime());
            binding.datePick.setText(selectedDate);

            binding.availableSlotText.setVisibility(View.VISIBLE);
            binding.recyclerViewSlots.setVisibility(View.VISIBLE);

            // Reset selected slot position when date changes
            selectedSlotPosition = RecyclerView.NO_POSITION;
            int restaurantId = intent.getIntExtra("id", 0);

            Call<SlotResponse> call = RetrofitClient
                    .getInstance()
                    .getRetrofitInterface()
                    .getAvailableSlots(restaurantId, selectedDate);

            call.enqueue(new Callback<SlotResponse>() {
                @Override
                public void onResponse(Call<SlotResponse> call, Response<SlotResponse> response) {
                    SlotResponse slotResponse = response.body();

                    if (response.isSuccessful()) {
                        slots = slotResponse.getAvailableSlots();
                        slotAdapter = new SlotAdapter(slots, RestaurantDetailActivity.this);
                        binding.recyclerViewSlots.setAdapter(slotAdapter);
                    } else {
                        Toast.makeText(RestaurantDetailActivity.this, "Error Code: " + response.code(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<SlotResponse> call, Throwable t) {
                    Toast.makeText(RestaurantDetailActivity.this, "Network error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        };

        // Horizontal Layout for Time slots.
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        binding.recyclerViewSlots.setLayoutManager(layoutManager);

        binding.btnReview.setOnClickListener(v -> {

            String numberOfPerson = binding.numberOfPerson.getText().toString();

            if (selectedDate == null || selectedDate.isEmpty()) {
                Toast.makeText(this, "Please select a date", Toast.LENGTH_SHORT).show();
                return;
            }

            if (selectedSlotPosition == RecyclerView.NO_POSITION) {
                Toast.makeText(RestaurantDetailActivity.this, "Please select a time slot!", Toast.LENGTH_SHORT).show();
                return;
            }

            if (numberOfPerson.isEmpty()) {
                binding.numberOfPerson.requestFocus();
                Toast.makeText(this, "Please enter number of persons", Toast.LENGTH_SHORT).show();
                return;
            }

            if (intent != null) {
                Intent confirmationIntent = new Intent(RestaurantDetailActivity.this, BookingReviewActivity.class);
                confirmationIntent.putExtra("id", intent.getIntExtra("id", 0));
                confirmationIntent.putExtra("restaurantName", intent.getStringExtra("name"));
                confirmationIntent.putExtra("image", intent.getStringExtra("image"));
                confirmationIntent.putExtra("date", selectedDate);
                confirmationIntent.putExtra("timeSlot", getSelectedSlot());
                confirmationIntent.putExtra("numberOfPerson", numberOfPerson);
                startActivity(confirmationIntent);
            }
        });

    }

    @Override
    public void onSlotClick(int position) {
        // Update selected slot position
        selectedSlotPosition = position;
        // Reset the number of persons to 0 when a new slot is selected
        binding.numberOfPerson.setText("1");
    }

    private String getSelectedSlot() {
        if (selectedSlotPosition != RecyclerView.NO_POSITION && slots != null && selectedSlotPosition < slots.size()) {
            SlotResponse.Slot selectedSlot = slots.get(selectedSlotPosition);
            // Format the selected slot data as needed (e.g., concatenate start and end time)
            return selectedSlot.getStartTime() + " - " + selectedSlot.getEndTime();
        }
        return null; // No slot selected
    }

    public void decrement(View view) {
        int currentValue = getValueFromEditText();
        // Ensure the new value is at least 1
        int newValue = Math.max(1, currentValue - 1);
        binding.numberOfPerson.setText(String.valueOf(newValue));
    }
    public void increment(View view) {
        if (selectedSlotPosition != RecyclerView.NO_POSITION && slots != null && selectedSlotPosition < slots.size()) {
            int currentValue = getValueFromEditText();
            int remainingCapacity = slots.get(selectedSlotPosition).getRemainingCapacity();
            // Ensure the new value is at most the remaining capacity
            int newValue = Math.min(remainingCapacity, currentValue + 1);
            binding.numberOfPerson.setText(String.valueOf(newValue));
        }
    }
    private int getValueFromEditText() {
        String valueStr = binding.numberOfPerson.getText().toString();
        if (valueStr.isEmpty()) {
            return 0; // Default value if the EditText is empty
        } else {
            int value = Integer.parseInt(valueStr);
            // Ensure the value is at least 1
            return Math.max(1, value);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle back button click here
        if (item.getItemId() == android.R.id.home) {
            // Navigate back to RestaurantListActivity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
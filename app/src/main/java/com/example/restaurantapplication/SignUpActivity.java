package com.example.restaurantapplication;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.restaurantapplication.ModelResponse.SignupResponse;
import com.example.restaurantapplication.databinding.ActivitySignUpBinding;

import org.json.JSONObject;

public class SignUpActivity extends AppCompatActivity {
    private ActivitySignUpBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignUpBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnSignUp.setOnClickListener(v -> {
            String name = binding.userName.getText().toString();
            String phoneNo = binding.userContactNumber.getText().toString();
            String email = binding.userEmail.getText().toString();
            String password = binding.userPassword.getText().toString();
            String contactNumber = binding.userContactNumber.getText().toString();

            if (name.isEmpty()){
                binding.userName.requestFocus();
                binding.userName.setError("Please enter your name");
                return;
            }
            if (email.isEmpty()){
                binding.userEmail.requestFocus();
                binding.userEmail.setError("Please enter your email");
                return;
            }
            // Check email format.
            if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                binding.userEmail.requestFocus();
                binding.userEmail.setError("Please Enter Correct Email");
                return;
            }
            // check phone number length
            if (phoneNo.length() != 10){
                binding.userContactNumber.requestFocus();
                binding.userContactNumber.setError("Please enter a valid contact number");
                return;
            }
            if (password.isEmpty()){
                binding.userPassword.requestFocus();
                binding.userPassword.setError("Please enter your password");
                return;
            }
            // Check password length
            if (password.length() < 8){
                binding.userPassword.requestFocus();
                binding.userPassword.setError("Password length must be at least 8 characters");
                return;
            }

            HashMap<String, String> map = new HashMap<>();
            map.put("name", name);
            map.put("email", email);
            map.put("password", password);
            map.put("contactNumber", contactNumber);

            Call<SignupResponse> call = RetrofitClient
                    .getInstance()
                    .getRetrofitInterface()
                    .executeSignup(map);

            call.enqueue(new Callback<SignupResponse>() {
                @Override
                public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                    SignupResponse signupResponse = response.body();
                    if (response.isSuccessful()) {
                        Toast.makeText(SignUpActivity.this, signupResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        try {
                            JSONObject errorObject = new JSONObject(response.errorBody().string());
                            if (errorObject.has("error")) {
                                String errorMessage = errorObject.getString("error");
                                Toast.makeText(SignUpActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(SignUpActivity.this, "Error Code: " + response.code(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SignupResponse> call, Throwable t) {
                    Toast.makeText(SignUpActivity.this, "Network error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        });

        binding.btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        binding.closeButton.setOnClickListener(v -> {
            Intent intent = new Intent(SignUpActivity.this,MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });
    }
}
package com.example.restaurantapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.restaurantapplication.R;

import java.util.ArrayList;
import java.util.List;

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.MyViewHolder> {
    private Context mContext;
    private List<String> locationList;
    private List<String> originalData;
    private OnItemClickListener onItemClickListener;

    public LocationListAdapter(Context mContext, List<String> locationList) {
        this.mContext = mContext;
        this.locationList = locationList;
        this.originalData = new ArrayList<>(locationList);
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        v = layoutInflater.inflate(R.layout.item_location, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String location = locationList.get(position);
        holder.location.setText(location);

        holder.itemView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView location;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            location = itemView.findViewById(R.id.location);
        }
    }

    // Search Filter
    public void filter(String query) {
        if (query.isEmpty()) {
            // If the query is empty, restore the original data
            locationList.clear();
            locationList.addAll(originalData);

        } else {
            // If there is a query, filter the data
            List<String> filteredList = new ArrayList<>();
            for (String location : originalData) {
                if (location.toLowerCase().contains(query.toLowerCase())) {
                    filteredList.add(location);
                }
            }
            locationList.clear();
            locationList.addAll(filteredList);
        }
        notifyDataSetChanged();
    }
}
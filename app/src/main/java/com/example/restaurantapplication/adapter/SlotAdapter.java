package com.example.restaurantapplication.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.restaurantapplication.ModelResponse.SlotResponse;
import com.example.restaurantapplication.R;

import java.util.List;

public class SlotAdapter extends RecyclerView.Adapter<SlotAdapter.SlotViewHolder> {
    private List<SlotResponse.Slot> slots;
    private OnSlotClickListener onSlotClickListener;
    private int selectedPosition = RecyclerView.NO_POSITION;

    public interface OnSlotClickListener {
        void onSlotClick(int position);
    }

    public SlotAdapter(List<SlotResponse.Slot> slots, OnSlotClickListener onSlotClickListener) {
        this.slots = slots;
        this.onSlotClickListener = onSlotClickListener;
    }

    @NonNull
    @Override
    public SlotViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_slot, parent, false);
        return new SlotViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SlotViewHolder holder, int position) {
        SlotResponse.Slot slot = slots.get(position);
        holder.startTimeTextView.setText(slot.getStartTime());
        holder.endTimeTextView.setText(slot.getEndTime());

        // Set background color based on slot availability
        int backgroundColor = slot.isReserved() ? R.color.colorReserved : R.color.colorAvailable;
        holder.itemView.setBackgroundResource(backgroundColor);

        // Highlight the selected slot
        if (position == selectedPosition) {
            holder.itemView.setBackgroundResource(R.drawable.slot_item_background);
        } else {
            holder.itemView.setBackgroundResource(R.drawable.selected_slot_bg);
        }

        holder.itemView.setOnClickListener(v -> {
            if (onSlotClickListener != null) {
                onSlotClickListener.onSlotClick(position);
                setSelectedPosition(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return slots.size();
    }

    public class SlotViewHolder extends RecyclerView.ViewHolder {
        private TextView startTimeTextView;
        private TextView endTimeTextView;

        public SlotViewHolder(@NonNull View itemView) {
            super(itemView);
            startTimeTextView = itemView.findViewById(R.id.startTimeTextView);
            endTimeTextView = itemView.findViewById(R.id.endTimeTextView);
        }
    }

    public void setSelectedPosition(int position) {
        this.selectedPosition = position;
        notifyDataSetChanged();
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }
}

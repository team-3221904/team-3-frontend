package com.example.restaurantapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.restaurantapplication.ModelResponse.RestaurantResponse;
import com.example.restaurantapplication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.MyViewHolder> {
    private Context mContext;
    private List<RestaurantResponse> restaurantList;
    private List<RestaurantResponse> originalData;
    private OnItemClickListener onItemClickListener;

    public RestaurantListAdapter(Context mContext, List<RestaurantResponse> restaurantList) {
        this.mContext = mContext;
        this.restaurantList = restaurantList;
        this.originalData = new ArrayList<>(restaurantList);
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        v = layoutInflater.inflate(R.layout.item_list,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(restaurantList.get(position).getImage()).into(holder.image);
        holder.cuisineType.setText(restaurantList.get(position).getCuisineType());
        holder.name.setText(restaurantList.get(position).getName());

        holder.itemView.setOnClickListener(view -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return restaurantList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView name;
        TextView cuisineType;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.listImage);
            name = itemView.findViewById(R.id.listName);
            cuisineType = itemView.findViewById(R.id.listCuisineType);
        }
    }

    // Search Filter
    public void filter(String query) {
        if (query.isEmpty()) {
            // If the query is empty, restore the original data
            restaurantList.clear();
            restaurantList.addAll(originalData);

        } else {
            // If there is a query, filter the data
            List<RestaurantResponse> filteredList = new ArrayList<>();
            for (RestaurantResponse data : originalData) {
                if (data.getCuisineType().toLowerCase().contains(query.toLowerCase()) || data.getName().toLowerCase().contains(query.toLowerCase())) {
                    filteredList.add(data);
                }
            }
            restaurantList.clear();
            restaurantList.addAll(filteredList);
        }
        notifyDataSetChanged();
    }
}
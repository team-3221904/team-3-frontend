package com.example.restaurantapplication;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import com.example.restaurantapplication.databinding.ActivityBookingSummaryBinding;

public class BookingSummaryActivity extends AppCompatActivity {
    private ActivityBookingSummaryBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBookingSummaryBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Intent intent = getIntent();
        if (intent!=null){
            String id = String.valueOf(intent.getIntExtra("id",0));
            String restaurantName = intent.getStringExtra("restaurantName");
            String userName = intent.getStringExtra("customerName");
            String timeSlot = intent.getStringExtra("start_time") + " - " +
                    intent.getStringExtra("end_time");
            String date = intent.getStringExtra("date");
            String noOfPerson = String.valueOf(intent.getIntExtra("no_of_person",0));
            String contactNumber = intent.getStringExtra("contactNumber");

            binding.restaurantName.setText(restaurantName);
            binding.date.setText(date);
            binding.timeSlot.setText(timeSlot);
            binding.numberOfPerson.setText(noOfPerson);
            binding.contactNumber.setText(contactNumber);
            binding.customerName.setText(userName);
            binding.bookingID.setText(id);
        }

        binding.btnBack.setOnClickListener(v -> {
            Intent intent1 = new Intent(BookingSummaryActivity.this, RestaurantListActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent1);
            finish();
        });
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent intent1 = new Intent(BookingSummaryActivity.this, RestaurantListActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent1);
                finish();
            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);
    }
}
package com.example.restaurantapplication;

import com.example.restaurantapplication.ModelResponse.BookingResponse;
import com.example.restaurantapplication.ModelResponse.LoginResponse;
import com.example.restaurantapplication.ModelResponse.RestaurantResponse;
import com.example.restaurantapplication.ModelResponse.SignupResponse;
import com.example.restaurantapplication.ModelResponse.ReservationResponse;
import com.example.restaurantapplication.ModelResponse.SlotResponse;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitInterface {
    @POST("auth/login")         // login endpoint
    Call<LoginResponse> executeLogin(@Body HashMap<String, String> map);

    @POST("auth/signup")        // signup endpoint
    Call<SignupResponse> executeSignup(@Body HashMap<String, String> map);

    @GET("location/locations") // location endpoint
    Call<List<String>> fetchLocations(@Header("Authorization") String authToken);

    @GET("location/locations/{city}") // restaurant list endpoint with dynamic city path parameter
    Call<List<RestaurantResponse>> fetchRestaurants(@Header("Authorization") String authToken, @Path("city") String cityName);

    @POST("booking/makeReservation")        // reservation endpoint
    Call<ReservationResponse> confirmBooking(@Header("Authorization") String authToken, @Body HashMap<String, Object> bookingDetails);

    @GET("slots/available-slots")   // available slots endpoint.
    Call<SlotResponse> getAvailableSlots(@Query("restaurantId") int restaurantId, @Query("date") String date);

    @GET("api/users/{userId}")  // booking details endpoint
    Call<BookingResponse> getBookingDetails(@Path("userId") int userId);
}
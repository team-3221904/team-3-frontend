package com.example.restaurantapplication;

import static android.content.Context.MODE_PRIVATE;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import androidx.core.content.res.ResourcesCompat;

public class AlarmReceiver extends BroadcastReceiver {
    private static final String CHANNEL_ID ="My Channel";
    private static final int NOTIFICATION_ID = 100;
    private static final int REQ_CODE = 100;
    @Override
    public void onReceive(Context context, Intent intent) {

        // Creating Pending intent
        Intent notificationIntent = new Intent(context,RestaurantListActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,REQ_CODE,notificationIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        // Drawable to Bitmap Drawable
        Drawable drawable = ResourcesCompat.getDrawable(context.getResources(),R.drawable.ic_large_notification_icon,null);
        BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
        Bitmap largeIcon = bitmapDrawable.getBitmap();

        // Different formats of Notification: Big Picture Style
        Notification.BigPictureStyle bigPictureStyle = new Notification.BigPictureStyle()
                .bigPicture(largeIcon)
                .bigLargeIcon(largeIcon)
                .setBigContentTitle("Big Content Title")
                .setSummaryText("Summary Text");

        // Inbox Style
        Notification.InboxStyle inboxStyle = new Notification.InboxStyle()
                .addLine("Click to Book Table")
                .setBigContentTitle("Check Nearby Restaurant Here.")
                .setSummaryText("Book Your Table!");

        // Notification Construction
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification foodNotification = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "New Channel", NotificationManager.IMPORTANCE_HIGH);
            nm.createNotificationChannel(channel);

            foodNotification = new Notification.Builder(context,CHANNEL_ID)
                    .setLargeIcon(largeIcon)
                    .setSmallIcon(R.drawable.ic_notification_icon)
                    .setContentText("Hey! " + getUserName(context) + " It's time for a meal.")
                    .setSubText("Food Time")
                    .setAutoCancel(true)    // Remove notification when user clicks.
//                    .setOngoing(true)   // User cannot cancel the notification
                    .setStyle(inboxStyle)   // Give style to notification
                    .setContentIntent(pendingIntent)
                    .setChannelId(CHANNEL_ID)
                    .build();
        }
        nm.notify(NOTIFICATION_ID, foodNotification);
    }

    private String getUserName(Context context){
        SharedPreferences pref = context.getSharedPreferences("userDetails",MODE_PRIVATE);
        String userName = pref.getString("name",null);
        return userName;
    }
}
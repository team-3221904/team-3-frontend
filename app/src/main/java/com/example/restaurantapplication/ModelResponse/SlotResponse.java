package com.example.restaurantapplication.ModelResponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SlotResponse {
    @SerializedName("availableSlots")
    private List<Slot> availableSlots;

    public List<Slot> getAvailableSlots() {
        return availableSlots;
    }

    public void setAvailableSlots(List<Slot> availableSlots) {
        this.availableSlots = availableSlots;
    }
    private static boolean isReserved;

    public static class Slot {
        @SerializedName("slot_id")
        private int slotId;
        @SerializedName("start_time")
        private String startTime;
        @SerializedName("end_time")
        private String endTime;
        @SerializedName("remaining_capacity")
        private int remainingCapacity;

        public int getSlotId() {
            return slotId;
        }

        public void setSlotId(int slotId) {
            this.slotId = slotId;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public int getRemainingCapacity() {
            return remainingCapacity;
        }

        public void setRemainingCapacity(int remainingCapacity) {
            this.remainingCapacity = remainingCapacity;
        }

        public boolean isReserved() {
            return isReserved;
        }

        public void setReserved(boolean reserved) {
            isReserved = reserved;
        }
    }
}

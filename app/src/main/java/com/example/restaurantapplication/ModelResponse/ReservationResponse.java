package com.example.restaurantapplication.ModelResponse;

import com.google.gson.annotations.SerializedName;

public class ReservationResponse {

    @SerializedName("id")
    private int id;

    @SerializedName("slot_id")
    private int slotId;

    @SerializedName("customer_id")
    private int customerId;

    @SerializedName("customer_name")
    private String customerName;

    @SerializedName("contact_number")
    private String contactNumber;

    @SerializedName("booking_date")
    private String bookingDate;

    @SerializedName("num_guests")
    private int numGuests;

    @SerializedName("error")
    private String error;

    // Getters and setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public int getNumGuests() {
        return numGuests;
    }

    public void setNumGuests(int numGuests) {
        this.numGuests = numGuests;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

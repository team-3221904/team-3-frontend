package com.example.restaurantapplication.ModelResponse;

import com.google.gson.annotations.SerializedName;

public class SignupResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("token")
    private String token;
    @SerializedName("error")
    private String error;

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }

    public String getError() {
        return error;
    }
}
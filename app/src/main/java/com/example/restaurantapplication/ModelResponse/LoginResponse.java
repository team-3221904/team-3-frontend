package com.example.restaurantapplication.ModelResponse;

import com.example.restaurantapplication.User;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("token")
    private String token;
    @SerializedName("user")
    private User user;
    @SerializedName("error")
    private String error;

    public LoginResponse(String message, String token, User user){
        message = this.message;
        token = this.token;
        user = this.user;
    }

    public String getMessage() {
        return message;
    }
    public String getToken() {
        return token;
    }
    public User getUser() {
        return user;
    }
}
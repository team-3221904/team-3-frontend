package com.example.restaurantapplication;

import org.junit.Before;
import org.junit.Test;
import java.util.List;
import static org.junit.Assert.*;
import com.example.restaurantapplication.ModelResponse.RestaurantResponse;
import com.example.restaurantapplication.adapter.RestaurantListAdapter;

public class FilterMethodTest {

    private RestaurantListAdapter restaurantListAdapter;
    private List<RestaurantResponse> testData;

    @Before
    public void setUp() {
        // Initialize the Adapter and test data before each test
        testData = createTestData();
        restaurantListAdapter = new RestaurantListAdapter(null, testData);
    }

    @Test
    public void testFilterWithEmptyQuery() {
        // Test filtering with an empty query
        restaurantListAdapter.filter("");
        assertEquals(testData.size(), restaurantListAdapter.getItemCount());
    }

    @Test
    public void testFilterWithMatchingLocation() {
        // Test filtering with a query matching a location
        restaurantListAdapter.filter("Location");
        assertEquals(2, restaurantListAdapter.getItemCount());
    }

    @Test
    public void testFilterWithNonMatchingQuery() {
        // Test filtering with a non-matching query
        restaurantListAdapter.filter("NonMatching");
        assertEquals(0, restaurantListAdapter.getItemCount());
    }

    @Test
    public void testFilterCaseInsensitive() {
        // Test filtering with a case-insensitive query
        restaurantListAdapter.filter("loCAtIon");
        assertEquals(2, restaurantListAdapter.getItemCount());
    }

    @Test
    public void testFilterRestoresOriginalData() {
        // Test that calling filter with an empty query restores original data
        restaurantListAdapter.filter("Location");
        restaurantListAdapter.filter("");
        assertEquals(testData.size(), restaurantListAdapter.getItemCount());
    }

    // Helper method to create test data
    private List<RestaurantResponse> createTestData() {
//        List<RestaurantResponse> testData = new ArrayList<>();
//        testData.add(new RestaurantResponse("1", "Name1", "Location1", "Image1"));
//        testData.add(new RestaurantResponse("2", "Name2", "Location2", "Image2"));
//        testData.add(new RestaurantResponse("3", "Name3", "Location3", "Image3"));
        return testData;
    }
}

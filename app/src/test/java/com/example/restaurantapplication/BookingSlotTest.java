package com.example.restaurantapplication;

import org.junit.Test;
import static org.junit.Assert.*;

public class BookingSlotTest {

    @Test
    public void testGetTime() {
        BookingSlot bookingSlot = new BookingSlot("12:00 PM", false);
        assertEquals("12:00 PM", bookingSlot.getTime());
    }

    @Test
    public void testSetTime() {
        BookingSlot bookingSlot = new BookingSlot("12:00 PM", false);
        bookingSlot.setTime("01:30 PM");
        assertEquals("01:30 PM", bookingSlot.getTime());
    }

    @Test
    public void testIsReserved() {
        BookingSlot bookingSlot = new BookingSlot("12:00 PM", false);
        assertFalse(bookingSlot.isReserved());
    }

    @Test
    public void testSetReserved() {
        BookingSlot bookingSlot = new BookingSlot("12:00 PM", false);
        bookingSlot.setReserved(true);
        assertTrue(bookingSlot.isReserved());
    }

    @Test
    public void testToString() {
        BookingSlot bookingSlot = new BookingSlot("12:00 PM", false);
        assertEquals("12:00 PM", bookingSlot.toString());
    }

    @Test
    public void testGetTimeSlot() {
        BookingSlot bookingSlot = new BookingSlot("12:00 PM", false);
        assertNull(bookingSlot.getTimeSlot());
    }
}

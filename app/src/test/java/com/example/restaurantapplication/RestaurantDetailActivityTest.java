package com.example.restaurantapplication;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class RestaurantDetailActivityTest {
    private RestaurantDetailActivity restaurantDetailActivity;

    @Before
    public void setUp() {
        // Initialize an instance of DetailedActivity before each test
        restaurantDetailActivity = new RestaurantDetailActivity();
    }

    @Test
    public void testGenerateSampleSlots() {
        // Ensure that generateSampleSlots returns a non-null list
        assertNotNull(restaurantDetailActivity.generateSampleSlots());
    }

    @Test
    public void testAddSlot() {
        // Create a list and add a slot to it
        List<BookingSlot> slots = new ArrayList<>();
        restaurantDetailActivity.addSlot(slots, 12);

        // Ensure that the list is not empty
        assertFalse(slots.isEmpty());

        // Ensure that the added slot has the correct properties
        BookingSlot addedSlot = slots.get(0);
        assertEquals("12:00 PM - 1:00 PM", addedSlot.getTimeSlot());
        assertFalse(addedSlot.isReserved());
    }

    @Test
    public void testOnSlotClick() {
        // Set up a sample slot list
        List<BookingSlot> slots = restaurantDetailActivity.generateSampleSlots();

        // Perform a slot click on the first slot
        restaurantDetailActivity.onSlotClick(0);

        // Ensure that the first slot is now reserved
        assertTrue(slots.get(0).isReserved());

        // Perform a slot click on the first slot again to deselect it
        restaurantDetailActivity.onSlotClick(0);

        // Ensure that the first slot is now not reserved
        assertFalse(slots.get(0).isReserved());
    }

}

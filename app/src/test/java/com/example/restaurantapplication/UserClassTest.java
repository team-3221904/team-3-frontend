package com.example.restaurantapplication;

import org.junit.Test;
import static org.junit.Assert.*;

public class UserClassTest {

    @Test
    public void testGetId() {
        // Arrange
        User user;
        // Act
        user = new User(1, "John Doe", "john@example.com","8081122453");
        // Assert
        assertEquals(1, user.getId());
    }

    @Test
    public void testSetId() {
        User user = new User(1, "John Doe", "john@example.com","808122453");
        user.setId(2);
        assertEquals(2, user.getId());
    }

    @Test
    public void testGetName() {
        User user = new User(1, "John Doe", "john@example.com","8081122453");
        assertEquals("John Doe", user.getName());
    }

    @Test
    public void testSetName() {
        User user = new User(1, "John Doe", "john@example.com","8081122453");
        user.setName("Jane Doe");
        assertEquals("Jane Doe", user.getName());
    }

    @Test
    public void testGetEmail() {
        User user = new User(1, "John Doe", "john@example.com","8081122453");
        assertEquals("john@example.com", user.getEmail());
    }

    // Additional Test Cases

    @Test
    public void testSetEmail() {
        User user = new User(1, "John Doe", "john@example.com","8081122453");
        user.setEmail("jane@example.com");
        assertEquals("jane@example.com", user.getEmail());
    }
}
